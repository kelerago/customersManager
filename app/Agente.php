<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agente extends Model
{
    protected $table = "agentes";

    protected $fillable = ['num_identificacion', 'nombre'];

    public function clientes(){

        return $this->hasMany('App\Cliente');
    }
}

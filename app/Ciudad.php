<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = "ciudades";

    protected $fillable = ['nombre', 'id_departamento'];

    public function departamento(){

        return $this->belongsTo('App\Departamento');
    }
}

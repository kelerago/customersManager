<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = "clientes";

    protected $fillable = [
        'nombre', 
        'num_identificacion', 
        'num_celular', 
        'direccion', 
        'id_agente', 
        'id_ciudad'       
    ];

    public function agente(){

        return $this->belongsTo('App\Agente');
    }

    public function usuario(){

        return $this->belongsTo('App\User');
    }
}

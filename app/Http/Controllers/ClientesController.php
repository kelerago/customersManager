<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cliente;
use App\Agente;
use App\Ciudad;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = DB::table('clientes')
        ->join('agentes', 'clientes.id_agente', '=', 'agentes.id')
        ->join('ciudades', 'clientes.id_ciudad', '=', 'ciudades.id')
        ->select('clientes.nombre AS nombre_cliente', 'clientes.num_identificacion', 'clientes.num_celular', 'clientes.direccion', 'agentes.nombre AS nombre_agente', 'ciudades.nombre AS nombre_ciudad', 'clientes.id AS id_cliente', 'agentes.id AS id_agente', 'ciudades.id AS id_ciudad')
        ->get();
        return $clientes;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agentes = Agente::all();
        $ciudades = Ciudad::all();

        return view('clientes.add', ['agentes' => $agentes, 'ciudades'  => $ciudades]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cliente = new Cliente( $request->all() );

        if ( $cliente->save() ){
            
            $result = 1; 

        } else{

            $result = 0;
        }

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        $cliente->fill( $request->all() );

        if( $cliente->save() ){

            $result = 1;
        } else{

            $result = 0;
        }

        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente =  Cliente::find($id);

        if( $cliente->delete() ){

            $result = 1;
        } else{

            $result = 0;
        }

        return $result;
    }
}

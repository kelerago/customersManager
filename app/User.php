<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "usuarios";
    
    protected $fillable = [
        'nombre', 
        'num_identificacion', 
        'username', 
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function clientes(){

        return $this->hasMany('App\Cliente');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('num_identificacion');
            $table->string('num_celular');
            $table->string('direccion');

            $table->integer('id_agente')->unsigned();
            $table->integer('id_ciudad')->unsigned();
            //$table->integer('id_usuario')->unsigned();
            $table->timestamps();

            $table->foreign('id_agente')->references('id')->on('agentes');
            $table->foreign('id_ciudad')->references('id')->on('ciudades');
            //$table->foreign('id_usuario')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}

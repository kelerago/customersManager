var app = angular.module('neuroMediaApp', []);

// Departamentos

app.controller('listarDepartamentos', function($http){    

    $http.get('/departamentos').then(response=>{

        this.departamentos = response.data;
    });

    this.agregarDepartamento = function(){

        $.ajax({

            url: '/departamentos',            
            type: 'post',
            data: { nombre : this.nombre }
        })
        .done(response => {

            if( response == 1 ){

                location.href="/departments"
            } else{

                alert('Ocurrio un error. Por favor intente nuevamente');
                location.href="/departments";
            }
        })
        .fail(response => {

            alert('Ocurrio un error. Por favor intente nuevamente');
            location.href="/departments";
        });
    };    
});

// Ciudades

app.controller('ciudades', function($http){    

    $http.get('/ciudades').then(response=>{

        this.ciudades = response.data;
    });    

    this.agregarCiudad = function(){

        $.ajax({

            url: '/ciudades',            
            type: 'post',
            data: { nombre : this.nombre, id_departamento : this.departamento }
        })
        .done(response => {

            if( response == 1 ){

                location.href="/cities"
            } else{

                alert('Ocurrio un error. Por favor intente nuevamente');
                location.href="/cities";
            }
        })
        .fail(response => {

            alert('Ocurrio un error. Por favor intente nuevamente');
            location.href="/cities";
        });
    };    
});

// Agentes

app.controller('agentes', function($http){

    $http.get('/agentes').then(response => {

        this.agentes = response.data;
        //console.log(this.agentes);
    });

    this.agregarAgentes = function(){

        $.ajax({

            url: '/agentes',            
            type: 'post',
            data: { nombre : this.nombre, num_identificacion : this.num_identificacion }
        })
        .done(response => {

            if( response == 1 ){

                location.href="/agents"
            } else{

                alert('Ocurrio un error. Por favor intente nuevamente');
                location.href="/agents";
            }
        })
        .fail(response => {

            alert('Ocurrio un error. Por favor intente nuevamente');
            location.href="/agents";
        });
    };
});



// Clientes

app.controller('clientes', function($http){    

    $http.get('/clientes').then(response=>{

        this.clientes = response.data;
    });

    $http.get('/agentes').then(response=>{

        this.agentes = response.data;        
    });

    $http.get('/ciudades').then(response=>{

        this.ciudades = response.data;        
    });

    this.obtenerInformacion = function(nombre, num_identificacion, num_celular, direccion, id_cliente, id_agente, id_ciudad){

        //this.id = id;
        this.nombre = nombre;
        this.num_identificacion = num_identificacion;
        this.num_celular = num_celular;
        this.direccion = direccion;
        this.cliente = id_cliente;
        this.agente = id_agente;
        this.ciudad = id_ciudad;
    };

    this.actualizarCliente = function(){

        $.ajax({

            url: '/clientes/update/' + this.cliente,
            data: { nombre: this.nombre, 
                    num_identificacion : this.num_identificacion, 
                    num_celular : this.num_celular, 
                    direccion : this.direccion, 
                    id_agente : this.agente,
                    id_ciudad : this.ciudad
                },
            type: 'get'
        })
        .done(response => {

            if( response == 1 ){

                location.href="/customers"
            } else{

                alert('Ocurrio un error. Por favor intente nuevamente');
                location.href="/customers";
            }
        })
        .fail(response => {

            alert('Ocurrio un error. Por favor intente nuevamente');
            location.href="/customers";
        });
    }

    this.eliminarCliente = function(id){
        
        this.id = id;

        $.ajax({

            url: '/clientes/destroy/' + this.id,            
            type: 'get'
        })
        .done(response => {

            if( response == 1 ){

                location.href="/customers"
            } else{

                alert('Ocurrio un error. Por favor intente nuevamente');
                location.href="/customers";
            }
        })
        .fail(response => {

            alert('Ocurrio un error. Por favor intente nuevamente');
            //location.href="/customers";
        });
    };

    this.agregarCliente = function(){

        $.ajax({

            url: '/clientes',            
            type: 'post',
            data: { nombre : this.nombre, 
                    num_identificacion : this.num_identificacion, 
                    num_celular : this.num_celular, 
                    direccion : this.direccion, 
                    id_agente : this.agente,
                    id_ciudad : this.ciudad
                }
        })
        .done(response => {

            if( response == 1 ){

                location.href="/customers"
            } else{

                alert('Ocurrio un error. Por favor intente nuevamente');
                location.href="/customers";
            }
        })
        .fail(response => {

            alert('Ocurrio un error. Por favor intente nuevamente');
            //location.href="/customers";
        });
    };    
});

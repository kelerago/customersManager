@extends('template.master')

@section('title', 'Agentes')

@section('action', 'Agregar')

@section('content')

<div class="card" ng-controller="agentes as ag">
    <div class="card-block">
        <form class="form-horizontal form-material">
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="nombre">Nombre</label>
                    <input ng-model="ag.nombre" id="nombre" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="nombre">Número de identificación</label>
                    <input class="form-control" ng-model="ag.num_identificacion" type="text" name="" id="num_identificación">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">                    
                    <button  ng-click="ag.agregarAgentes()" class="btn btn-success">Agregar</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
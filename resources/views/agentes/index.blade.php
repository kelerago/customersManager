@extends('template.master')

@section('title', 'Agentes')

@section('action', 'Lista')

@section('content')

    <div class="card" ng-controller="agentes as ag">
        <div class="card-block">
        <a class="addIcon" href="{{route('agentes.create')}}"> <i class="mdi mdi-library-plus"></i> Agregar Agentes  </a>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Numero de identificación</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="agente in ag.agentes">
                            <td> @verbatim {{ agente.nombre }} @endverbatim </td>
                            <td> @verbatim {{ agente.num_identificacion }} @endverbatim </td>                                                 
                        </tr>                                          
                    </tbody>
                </table>
            </div>
        </div>        
    </div>    

@endsection
@extends('template.master')

@section('title', 'Ciudades')

@section('action', 'Agregar')

@section('content')

<div class="card" ng-controller="ciudades as ct">
    <div class="card-block">
        <form class="form-horizontal form-material">
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="nombre">Nombre</label>
                    <input ng-model="ct.nombre" id="nombre" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="nombre">Departamento</label>
                    <select ng-model="ct.departamento" id="departamento"  class="form-control">
                        @foreach($departamentos as $departamento)
                            <option value="{{$departamento->id}}"> {{$departamento->nombre}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">                    
                    <button  ng-click="ct.agregarCiudad()" class="btn btn-success">Agregar</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
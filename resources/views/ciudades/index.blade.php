@extends('template.master')

@section('title', 'Ciudades')

@section('action', 'Lista')

@section('content')

    <div class="card" ng-controller="ciudades as ct">
        <div class="card-block">
        <a class="addIcon" href="{{route('ciudades.create')}}"> <i class="mdi mdi-library-plus"></i> Agregar Ciudad  </a>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Departamento</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="ciudad in ct.ciudades">
                            <td> @verbatim {{ ciudad.nombre_ciudad }} @endverbatim </td>
                            <td> @verbatim {{ ciudad.nombre_departamento }} @endverbatim </td>                                                 
                        </tr>                                          
                    </tbody>
                </table>
            </div>
        </div>        
    </div>    

@endsection
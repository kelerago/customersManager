@extends('template.master')

@section('title', 'Clientes')

@section('action', 'Agregar')

@section('content')

<div class="card" ng-controller="clientes as cl">
    <div class="card-block">
        <form class="form-horizontal form-material">
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="nombre">Nombre</label>
                    <input ng-model="cl.nombre" id="nombre" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="num_identificacion">Número de identificación</label>
                    <input ng-model="cl.num_identificacion" id="num_identificacion" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="num_celular">Número de celular</label>
                    <input ng-model="cl.num_celular" id="num_celular" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="direccion">Dirección</label>
                    <input ng-model="cl.direccion" id="direccion" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="agente">Agente</label>
                    <select name="" id="agente" class="form-control" ng-model="cl.agente">
                        @foreach($agentes as $agente)
                            <option value="{{$agente->id}}"> {{$agente->nombre}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="ciudad">Ciudad</label>
                    <select name="" id="ciudad" class="form-control" ng-model="cl.ciudad">
                        @foreach($ciudades as $ciudad)
                            <option value="{{$ciudad->id}}"> {{$ciudad->nombre}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">                    
                    <button  ng-click="cl.agregarCliente()" class="btn btn-success">Agregar</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@extends('template.master')

@section('title', 'Clientes')

@section('action', 'Lista')

@section('content')

    <div class="card" ng-controller="clientes as cl">
        <div class="card-block">
            <a class="addIcon" href="{{route('clientes.create')}}"> <i class="mdi mdi-library-plus"></i> Agregar Cliente </a>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nombre</th>    
                            <th>Número de identificación</th>
                            <th>Número de celular</th>
                            <th>Dirección</th>
                            <th>Agente</th>
                            <th>Ciudad</th>           
                            <th>Acciones</th>             
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="cliente in cl.clientes">
                            <td> @verbatim {{ cliente.nombre_cliente }} @endverbatim </td>
                            <td> @verbatim {{ cliente.num_identificacion }} @endverbatim </td>
                            <td> @verbatim {{ cliente.num_celular }} @endverbatim </td>        
                            <td> @verbatim {{ cliente.direccion }} @endverbatim </td>     
                            <td> @verbatim {{ cliente.nombre_agente }} @endverbatim </td>   
                            <td> @verbatim {{ cliente.nombre_ciudad }} @endverbatim </td>   
                            <td>
                                <button class="btn" data-toggle="modal" data-target="#modalClientes" ng-click="cl.obtenerInformacion(cliente.nombre_cliente, cliente.num_identificacion, cliente.num_celular, cliente.direccion, cliente.id_cliente, cliente.id_agente, cliente.id_ciudad)"><i class="mdi mdi-grease-pencil"></i></button>
                                <button class="btn" ng-click="cl.eliminarCliente(cliente.id_cliente)"><i class="mdi mdi-delete"></i></button>
                            </td>                                
                        </tr>                                          
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal de edición -->

        <div class="modal fade" id="modalClientes">
            <div class="modal-dialog">
                <div class="modal-dialog">
                    <div align="right" class="modal-header">
                        <button class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-content">
                        <div class="container">
                            <h3> Edición de Cliente </h3>
                            <form class="form-horizontal form-material">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="nombre">Nombre</label>
                                        <input id="nombre" type="text" class="form-control" ng-model="cl.nombre">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="num_identificacion">Número de identificación</label>
                                        <input id="num_identificacion" type="text" class="form-control" ng-model="cl.num_identificacion">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="num_celular">Número de celular</label>
                                        <input id="num_celular" type="text" class="form-control" ng-model="cl.num_celular">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="direccion">Dirección</label>
                                        <input id="direccion" type="text" class="form-control" ng-model="cl.direccion">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="agente">Agente</label>
                                        <select id="agente" class="form-control" ng-model="cl.agente">
                                            <option ng-repeat="agente in cl.agentes" @verbatim value="{{ agente.id }}">  {{ agente.nombre }} @endverbatim </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for="ciudad">Ciudad</label>
                                        <select id="ciudad" class="form-control" ng-model="cl.ciudad">
                                            <option ng-repeat="ciudad in cl.ciudades" @verbatim value="{{ ciudad.id_ciudad }}">  {{ ciudad.nombre_ciudad }} @endverbatim </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button class="btn btn-success" ng-click="cl.actualizarCliente()">Editar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    

@endsection
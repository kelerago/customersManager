@extends('template.master')

@section('title', 'Departamentos')

@section('action', 'Agregar')

@section('content')

<div class="card" ng-controller="listarDepartamentos as ld">
    <div class="card-block">
        <form class="form-horizontal form-material">
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="nombre">Nombre</label>
                    <input ng-model="ld.nombre" id="nombre" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">                    
                    <button  ng-click="ld.agregarDepartamento()" class="btn btn-success">Agregar</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@extends('template.master')

@section('title', 'Departamentos')

@section('action', 'Lista')

@section('content')

    <div class="card" ng-controller="listarDepartamentos as ld">
        <div class="card-block">
            <a class="addIcon" href="{{route('departamentos.create')}}"> <i class="mdi mdi-library-plus"></i> Agregar Departamento  </a>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nombre</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="departamento in ld.departamentos">
                            <td> @verbatim {{ departamento.nombre }} @endverbatim </td>                                                 
                        </tr>                                          
                    </tbody>
                </table>
            </div>
        </div>        
    </div>


    

@endsection
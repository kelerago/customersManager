@extends('template.master')

@section('title', 'Home')

@section('action', 'Bienvenida')

@section('content')

    <div class="card" ng-controller="listarDepartamentos as ld">
        <div class="card-block">

            <p id="bienvenida">
                Sean bienvenidos a este sistema administrador de clientes. Espero que sea de su agrado.
            </p>

            <div align="center">
                <img id="config" src="{{ asset('assets/images/config.svg') }}" alt="">
            </div>

        </div>
    </div>

@endsection
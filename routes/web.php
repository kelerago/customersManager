<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Departamentos

Route::resource('departamentos', 'DepartamentosController');   

Route::get('departments', function(){

    return view('departamentos.index');
});

// Ciudades

Route::resource('ciudades', 'CiudadesController');

Route::get('cities', function(){

    return view('ciudades.index');
});

// Agentes

Route::resource('agentes', 'AgentesController');   

Route::get('agents', function(){

    return view('agentes.index');
});

// Clientes

Route::resource('clientes', 'ClientesController');

Route::get('clientes/update/{cliente}', [

    'uses' => 'ClientesController@update',
    'as' => 'clientes.update'
]); 

Route::get('clientes/destroy/{cliente}', [

    'uses' => 'ClientesController@destroy',
    'as' => 'clientes.destroy'
]); 

Route::get('customers', function(){

    return view('clientes.index');
});
